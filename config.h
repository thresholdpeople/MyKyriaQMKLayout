/* Copyright 2019 Thomas Baart <thomas@splitkb.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#ifdef RGBLIGHT_ENABLE
  #define RGBLIGHT_ANIMATIONS
  #define RGBLIGHT_HUE_STEP 8
  #define RGBLIGHT_SAT_STEP 8
  #define RGBLIGHT_VAL_STEP 8
  #define RGBLIGHT_LIMIT_VAL 150
#endif

// Debounce defers to /kyria/rev1/config.h, so is commented out here.
// #define DEBOUNCE 5  // switched from 13 back to 5, same as kyria/rev1/config.h

// https://docs.qmk.fm/#%2Ffeature_mouse_keys?id=mouse-keys
#define MOUSEKEY_MAX_SPEED 10
#define MOUSEKEY_TIME_TO_MAX 40
#define MOUSEKEY_MOVE_DELTA 5
#define MOUSEKEY_INTERVAL 32

// #define COMBO_COUNT 1   // https://docs.qmk.fm/#%2Ffeature_combo?id=combos

// The Leader key allows to flexibly assign macros to key sequences.
// #ifdef LEADER_ENABLE
//   #define LEADER_PER_KEY_TIMING
//   #define LEADER_TIMEOUT 350
// #endif

// #ifdef COMBO_ENABLE
//   #define TAPPING_TERM 200
//   #define IGNORE_MOD_TAP_INTERRUPT // this makes it possible to do rolling combos (zx) with keys that convert to other keys on hold (z becomes ctrl when you hold it, and when this option isn't enabled, z rapidly followed by x actually sends Ctrl-x. That's bad.)
// #endif

// Allows media codes to properly register in macros and rotary encoder code
// #define TAP_CODE_DELAY 10

// https://docs.qmk.fm/#%2Ftap_hold?id=tapping-force-hold
 // #define TAPPING_FORCE_HOLD