MCU = atmega32u4				# MCU name
BOOTLOADER = atmel-dfu		# Bootloader selection

OLED_DRIVER_ENABLE = no   		# Enables the use of OLED displays
ENCODER_ENABLE = no      		# Enables the use of one or more encoders
RGBLIGHT_ENABLE = no     		# Enable keyboard RGB underglow: https://beta.docs.qmk.fm/using-qmk/hardware-features/lighting/feature_rgblight

LEADER_ENABLE = no        		# Enable the Leader Key feature: https://beta.docs.qmk.fm/using-qmk/advanced-keycodes/feature_leader_key
# COMBO_ENABLE = yes			# Enable combo key feature: https://docs.qmk.fm/#%2Ffeature_combo?id=combos

MOUSEKEY_ENABLE = yes			# https://docs.qmk.fm/#%2Ffeature_mouse_keys=


# DEBOUNCE_TYPE  = custom		# https://beta.docs.qmk.fm/using-qmk/software-features/feature_debounce_type
# SRC += debounce.c 			# default algorithm used, with debounce time defined in config.h (required mod to /kyria/rev1/config.h to overwrite master def)
DEBOUNCE_TYPE = sym_defer_g