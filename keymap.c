#include QMK_KEYBOARD_H

enum layers {
    _QWERTY = 0,
    _NUML = 1,
    _NUMR = 2,
    _NAV = 3,
    _NAVNM = 4,
    _NUMNV = 5,
    _MOUSE = 6
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
/*
 *
 * Base Layer: QWERTY
 *
 * ,-------------------------------------------.                              ,-------------------------------------------.
 * |Tab     |   Q  |   W  |   E  |   R  |   T  |                              |   Y  |   U  |   I  |   O  |   P  |  \  |  |
 * |--------+------+------+------+------+------|                 old ScrnSht: |------+------+------+------+------+--------|
 * |Bckspc  |   A  |   S  |   D  |   F  |   G  |                 SGUI(KC_4)   |   H  |   J  |   K  |   L  | ;  : |  ' "   |
 * |--------+------+------+------+------+------+-------------.  ,-------------+------+------+------+------+------+--------|
 * |Esc/ `~ |   Z  |   X  |   C  |   V  |   B  | NUM  | Enter|  |Screen|  `   |   N  |   M  | ,  < | . >  | /  ? |  - _   |
 * |        |      |      |      |      |      | SYM  |      |  | Shot |      |      |      |      |      |      |        |
 * `----------------------+------+------+------+------+------|  |------+------+------+------+------+----------------------'
 *                        | Ctrl |Option| Cmd  | Shift| Space|  | Enter| NUM  | NAV  | [  { | ]  } |
 *                        |      |      |      |      |      |  |      | SYM  |      |      |      |
 *                        `----------------------------------'  `----------------------------------'
 *
 */
    [_QWERTY] = LAYOUT(
      KC_TAB,    KC_Q,    KC_W,    KC_E,    KC_R,    KC_T,                                                          KC_Y,       KC_U,       KC_I,       KC_O,   KC_P,       KC_BSLS,
      KC_BSPC,   KC_A,    KC_S,    KC_D,    KC_F,    KC_G,                                                          KC_H,       KC_J,       KC_K,       KC_L,   KC_SCLN,    KC_QUOT,
      KC_GESC,   KC_Z,    KC_X,    KC_C,    KC_V,    KC_B,    MO(_NUML),  KC_ENT,         G(A(S(KC_X))), KC_GRAVE,  KC_N,       KC_M,       KC_COMM,    KC_DOT, KC_SLSH,    KC_MINS,
                                   KC_LCTL, KC_LALT, KC_LGUI, KC_LSFT,    KC_SPC,         KC_ENT,        MO(_NUMR), MO(_NAV),   KC_LBRC,    KC_RBRC
    ),
/*
 *
 * Numbers & Symbols Left Hand Access Layer:  access from base layer (left side)
 *
 * ,-------------------------------------------.                              ,-------------------------------------------.
 * | Tab    |   (  |   )  |   [  |   ]  |   \  |                              |   =  |   +  |   *  |   -  |   /  |  |     |
 * |--------+------+------+------+------+------|                              |------+------+------+------+------+--------|
 * | Bckspc |   1  |   2  |   3  |   4  |   5  |                              |   6  |   7  |   8  |   9  |   0  |  "     |
 * |--------+------+------+------+------+------+-------------.  ,-------------+------+------+------+------+------+--------|
 * | ~      |   !  |   @  |   #  |   $  |   %  | RSRVD| Enter|  |Screen|  `   |   ^  |   &  |   ,  |   .  |   ?  |  -     |
 * |        |      |      |      |      |      | LYR  |      |  |Shot  |      |      |      |      |      |      |        |
 * `----------------------+------+------+------+------+------|  |------+------+------+------+------+----------------------'
 *                        | Ctrl |Option| Cmd  | NAV  | Space|  | Enter| Shift| NAV  |  <   |  >   |
 *                        |      |      |      |      |      |  |      |      |      |      |      |
 *                        `----------------------------------'  `----------------------------------'
 *
 */
    [_NUML] = LAYOUT(
      _______,   KC_LPRN, KC_RPRN, KC_LBRC, KC_RBRC, KC_BSLS,                                                       KC_EQL,     KC_PLUS,    KC_ASTR,    KC_MINS,    KC_SLSH,    KC_PIPE,
      _______,   KC_1,    KC_2,    KC_3,    KC_4,    KC_5,                                                          KC_6,       KC_7,       KC_8,       KC_9,       KC_0,       KC_DQUO,
      KC_TILD,   KC_EXLM, KC_AT,   KC_HASH, KC_DLR,  KC_PERC, _______,  _______,          _______,      _______,    KC_CIRC,    KC_AMPR,    _______,    _______,    _______,    _______,
                                   _______, _______, _______, MO(_NAV), _______,          _______,      KC_LSFT,    MO(_NAV),   KC_LT,      KC_GT
    ),
/*
 *
 * Numbers & Symbols Right Hand Access Layer:   access from base layer (right side)
 *
 * ,-------------------------------------------.                              ,-------------------------------------------.
 * | Tab    |   (  |   )  |   [  |   ]  |   \  |                              |   =  |   +  |   *  |   -  |   /  |  |     |
 * |--------+------+------+------+------+------|                              |------+------+------+------+------+--------|
 * | Bckspc |   1  |   2  |   3  |   4  |   5  |                              |   6  |   7  |   8  |   9  |   0  |  "     |
 * |--------+------+------+------+------+------+-------------.  ,-------------+------+------+------+------+------+--------|
 * | ~      |   !  |   @  |   #  |   $  |   %  |NUMNAV| Enter|  |Screen|  `   |   ^  |   &  |   ,  |   .  |   ?  |  -     |
 * |        |      |      |      |      |      |      |      |  |Shot  |      |      |      |      |      |      |        |
 * `----------------------+------+------+------+------+------|  |------+------+------+------+------+----------------------'
 *                        | Ctrl |Option| Cmd  | Shift| Space|  | Enter| RSRVD| NAV  |  <   |  >   |
 *                        |      |      |      |      |      |  |      | LYR  |      |      |      |
 *                        `----------------------------------'  `----------------------------------'
 *
 */
    [_NUMR] = LAYOUT(
      _______,   KC_LPRN, KC_RPRN, KC_LBRC, KC_RBRC, KC_BSLS,                                                         KC_EQL,     KC_PLUS,    KC_ASTR,    KC_MINS,    KC_SLSH,    KC_PIPE,
      _______,   KC_1,    KC_2,    KC_3,    KC_4,    KC_5,                                                            KC_6,       KC_7,       KC_8,       KC_9,       KC_0,       KC_DQUO,
      KC_TILD,   KC_EXLM, KC_AT,   KC_HASH, KC_DLR,  KC_PERC, MO(_NUMNV), _______,          _______,      _______,    KC_CIRC,    KC_AMPR,    _______,    _______,    _______,    _______,
                                   _______, _______, _______, KC_LSFT,    _______,          _______,      _______,    MO(_NAV),   KC_LT,      KC_GT
    ),
/*
 *
 * Navigation & Numberpad:    access from numbers & symbols layer (requires both hands)
 *
 * ,--------------------------------------------.                              ,-------------------------------------------.
 * | Tab    | Home |      |  Up  |      | Pg Up |                              | =  + |   +  |   *  | -  _ | /  ? |  %     |
 * |--------+------+------+------+------+-------|                              |------+------+------+------+------+--------|
 * | Bckspc | End  | Left | Down | Right| Pg Dwn|                              |   6  |   7  |   8  |   9  |   0  |  ^     |
 * |--------+------+------+------+------+-------+-------------.  ,-------------+------+------+------+------+------+--------|
 * | Esc/ `~| Back | Fwd  | Left | Right|       | RSRVD| Enter|  |Screen|  `   |   1  |   2  |   3  |   4  |   5  |  #     |
 * |        |      |      | Tab  | Tab  |       | LYR  |      |  |Shot  |      |      |      |      |      |      |        |
 * `----------------------+------+------+-------+------+------|  |------+------+------+------+------+----------------------'
 *                        | Ctrl |Option| Cmd   | Shift| Space|  | Enter| RSRVD|NAVNUM| [  { | ]  } |
 *                        |      |      |       |      |      |  |      | LYR  |      |      |      |
 *                        `-----------------------------------'  `----------------------------------'
 *
 */
    [_NAVNM] = LAYOUT(
      _______,    XXXXXXX,    _______,    XXXXXXX,    _______,    _______,                                                          KC_EQL,     KC_PLUS,    KC_ASTR,    KC_MINS,    KC_SLSH,    KC_PERC,
      _______,    _______,    _______,    _______,    _______,    _______,                                                          KC_6,       KC_7,       KC_8,       KC_9,       KC_0,       KC_CIRC,
      _______,    _______,    _______,    _______,    _______,    XXXXXXX,    _______,    _______,          _______,    _______,    KC_1,       KC_2,       KC_3,       KC_4,       KC_5,       KC_HASH,
                                          _______,    _______,    _______,    KC_LSFT,    _______,          _______,    _______,    _______,    _______,    _______
    ),
/*
 *
 * Navigation Layer:    access from base layer (and others)
 *
 * ,--------------------------------------------.                              ,-------------------------------------------.
 * | Tab    | Home |      |  Up  |      | Pg Up |                              | Home |      |  Up  |      | Pg Up|  Ctrl  |
 * |--------+------+------+------+------+-------|                              |------+------+------+------+------+--------|
 * | Bckspc | End  | Left | Down | Right| Pg Dwn|                              | End  | Left | Down | Right|Pg Dwn|  Cmd   |
 * |--------+------+------+------+------+-------+-------------.  ,-------------+------+------+------+------+------+--------|
 * | Esc/ `~| Back | Fwd  | Left | Right| MOUSE | RSRVD| Enter|  |Screen|  `   | Back | Fwd  | Left | Right|      | Option |
 * |        |      |      | Tab  | Tab  |       | LYR  |      |  |Shot  |      |      |      | Tab  | Tab  |      |        |
 * `----------------------+------+------+-------+------+------|  |------+------+------+------+------+----------------------'
 *                        | Ctrl |Option| Cmd   | Shift| Space|  | Enter| MOUSE| RSRVD| [  { | ]  } |
 *                        |      |      |       |      |      |  |      |      | LYR  |      |      |
 *                        `-----------------------------------'  `----------------------------------'
 *
 */
    [_NAV] = LAYOUT(
      KC_TAB,     KC_HOME,      XXXXXXX,        KC_UP,          XXXXXXX,        KC_PGUP,                                                             KC_HOME,        XXXXXXX,        KC_UP,         XXXXXXX,        KC_PGUP,  KC_LCTL,
      KC_BSPC,    KC_END,       KC_LEFT,        KC_DOWN,        KC_RIGHT,       KC_PGDN,                                                             KC_END,         KC_LEFT,        KC_DOWN,       KC_RIGHT,       KC_PGDN,  KC_LGUI,
      _______,    LGUI(KC_LBRC),LGUI(KC_RBRC),  LAG(KC_LEFT),   LAG(KC_RIGHT),  MO(_MOUSE), MO(_NAVNM),   _______,          _______,    _______,     LGUI(KC_LBRC),  LGUI(KC_RBRC),  LAG(KC_LEFT),  LAG(KC_RIGHT),  XXXXXXX,  KC_LALT,
                                                _______,        _______,        _______,    _______,      _______,          _______,    MO(_MOUSE),  _______,        KC_LBRC,        KC_RBRC
    ),
/*
 *
 * Numberpad & Navigation:    access from nav layer (right side)
 *
 * ,--------------------------------------------.                              ,-------------------------------------------.
 * | %      | =  + |  +   |  *   | -  _ | /  ?  |                              | Home |      |  Up  |      | Pg Up|  Ctrl  |
 * |--------+------+------+------+------+-------|                              |------+------+------+------+------+--------|
 * | ^      |  6   |  7   |  8   |  9   |  0    |                              | End  | Left | Down | Right|Pg Dwn|  Cmd   |
 * |--------+------+------+------+------+-------+-------------.  ,-------------+------+------+------+------+------+--------|
 * | #      |  1   |  2   |  3   |  4   |  5    | RSRVD| Enter|  |Screen|  `   | Back | Fwd  | Left | Right|      | Option |
 * |        |      |      |      |      |       | LYR  |      |  |Shot  |      |      |      | Tab  | Tab  |      |        |
 * `----------------------+------+------+-------+------+------|  |------+------+------+------+------+----------------------'
 *                        | Ctrl |Option| Cmd   | Shift| Space|  | Enter| RSRVD| RSRVD| [  { | ]  } |
 *                        |      |      |       |      |      |  |      | LYR  | LYR  |      |      |
 *                        `-----------------------------------'  `----------------------------------'
 *
 */
    [_NUMNV] = LAYOUT(
      KC_PERC,    KC_EQL,   KC_PLUS,    KC_ASTR,    KC_MINS,    KC_SLSH,                                                          KC_HOME,        XXXXXXX,        KC_UP,          XXXXXXX,        KC_PGUP,    KC_LCTL,
      KC_CIRC,    KC_6,     KC_7,       KC_8,       KC_9,       KC_0,                                                             KC_END,         KC_LEFT,        KC_DOWN,        KC_RIGHT,       KC_PGDN,    KC_LGUI,
      KC_HASH,    KC_1,     KC_2,       KC_3,       KC_4,       KC_5,       _______,    _______,          _______,    _______,    LGUI(KC_LBRC),  LGUI(KC_RBRC),  LAG(KC_LEFT),   LAG(KC_RIGHT),  XXXXXXX,    KC_LALT,
                                        _______,    _______,    _______,    _______,    _______,          _______,    _______,    _______,        _______,        _______
    ),
/*
 *
 * Mouse Nav Layer:   access from nav layer (both sides)
 *
 * ,--------------------------------------------.                              ,-------------------------------------------.
 * | Tab    | Home |      |  Up  |      | WhlUp |                              | WhlUp|      |  Up  |      | Pg Up|  Ctrl  |
 * |--------+------+------+------+------+-------|                              |------+------+------+------+------+--------|
 * | Bckspc | End  | Left | Down | Right| WhlDwn|                              |WhlDwn| Left | Down | Right|Pg Dwn|  Cmd   |
 * |--------+------+------+------+------+-------+-------------.  ,-------------+------+------+------+------+------+--------|
 * | Esc/ `~|      | Left | Right|      | RSRVD | RSRVD| Enter|  |Screen|  `   |      | Left | Right|      |      | Option |
 * |        |      | Click| Click|      | LYR   | LYR  |      |  |Shot  |      |      | Click| Click|      |      |        |
 * `----------------------+------+------+-------+------+------|  |------+------+------+------+------+----------------------'
 *                        | Ctrl |Option| Cmd   | RSRVD| Space|  | Enter| RSRVD| RSRVD| [  { | ]  } |
 *                        |      |      |       | LYR  |      |  |      | LYR  | LYR  |      |      |
 *                        `-----------------------------------'  `----------------------------------'
 *
 */
    [_MOUSE] = LAYOUT(
      _______,  KC_HOME,    XXXXXXX,   KC_MS_U,  KC_NO,    KC_WH_U,                                                     KC_WH_U,  KC_NO,    KC_MS_U,  KC_NO,    KC_PGUP,  KC_LCTL,
      _______,  KC_END,     KC_MS_L,   KC_MS_D,  KC_MS_R,  KC_WH_D,                                                     KC_WH_D,  KC_MS_L,  KC_MS_D,  KC_MS_R,  KC_PGDN,  KC_LGUI,
      _______,  XXXXXXX,    KC_BTN1,   KC_BTN2,  XXXXXXX,  _______,  _______,  _______,          _______,    _______,   XXXXXXX,  KC_BTN1,  KC_BTN2,  XXXXXXX,  XXXXXXX,  KC_LALT,
                                       _______,  _______,  _______,  _______,  _______,          _______,    _______,   _______,  KC_LBRC,  KC_RBRC
    ),   
// /*
//  * Layer template
//  *
//  * ,-------------------------------------------.                              ,-------------------------------------------.
//  * |        |      |      |      |      |      |                              |      |      |      |      |      |        |
//  * |--------+------+------+------+------+------|                              |------+------+------+------+------+--------|
//  * |        |      |      |      |      |      |                              |      |      |      |      |      |        |
//  * |--------+------+------+------+------+------+-------------.  ,-------------+------+------+------+------+------+--------|
//  * |        |      |      |      |      |      |      |      |  |      |      |      |      |      |      |      |        |
//  * `----------------------+------+------+------+------+------|  |------+------+------+------+------+----------------------'
//  *                        |      |      |      |      |      |  |      |      |      |      |      |
//  *                        |      |      |      |      |      |  |      |      |      |      |      |
//  *                        `----------------------------------'  `----------------------------------'
//  */
//     [_LAYERINDEX] = LAYOUT(
//       _______, _______, _______, _______, _______, _______,                                     _______, _______, _______, _______, _______, _______,
//       _______, _______, _______, _______, _______, _______,                                     _______, _______, _______, _______, _______, _______,
//       _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
//                                  _______, _______, _______, _______, _______, _______, _______, _______, _______, _______
//     ),
};

// layer_state_t layer_state_set_user(layer_state_t state) {
//     return update_tri_layer_state(state, _LOWER, _RAISE, _ADJUST);
// }


// https://docs.qmk.fm/#/feature_advanced_keycodes?id=shift-backspace-for-delete
// Shift + Backspace = Delete
// Initialize variable holding the binary
// representation of active modifiers.
uint8_t mod_state;
bool process_record_user(uint16_t keycode, keyrecord_t *record) {
    // Store the current modifier state in the variable for later reference
    mod_state = get_mods();
    switch (keycode) {

    case KC_BSPC:
        {
        // Initialize a boolean variable that keeps track
        // of the delete key status: registered or not?
        static bool delkey_registered;
        if (record->event.pressed) {
            // Detect the activation of either shift keys
            if (mod_state & MOD_MASK_SHIFT) {
                // First temporarily canceling both shifts so that
                // shift isn't applied to the KC_DEL keycode
                del_mods(MOD_MASK_SHIFT);
                register_code(KC_DEL);
                // Update the boolean variable to reflect the status of KC_DEL
                delkey_registered = true;
                // Reapplying modifier state so that the held shift key(s)
                // still work even after having tapped the Backspace/Delete key.
                set_mods(mod_state);
                return false;
            }
        } else { // on release of KC_BSPC
            // In case KC_DEL is still being sent even after the release of KC_BSPC
            if (delkey_registered) {
                unregister_code(KC_DEL);
                delkey_registered = false;
                return false;
            }
        }
        // Let QMK process the KC_BSPC keycode as usual outside of shift
        return true;
    }

    }
    return true;
};

// https://docs.qmk.fm/#/feature_combo


// Set the RGB Underglow lights based on the layer:

// https://beta.docs.qmk.fm/using-qmk/guides/custom_quantum_functions#layer-change-code-id-layer-change-code
// This runs code every time that the layers get changed. This can be useful for layer indication, or custom layer handling.
/*
layer_state_t layer_state_set_user(layer_state_t state) {
    switch (get_highest_layer(state)) {
    case _NUMSYML:
        rgblight_setrgb (0x00,  0xFF, 0xFF);
        break;
    case _NUMSYMR:
        rgblight_setrgb (0x00,  0xFF, 0xFF);
        break;
    case _NAV:
        rgblight_setrgb (0xFF,  0x99, 0x33);
        break;
    case _NAVNUML:
        rgblight_setrgb (0xCC,  0x00, 0x00);
        break;
    case _NAVNUMR:
        rgblight_setrgb (0xFF,  0x00, 0x7F);
        break;
    case _NUMSYMD:
        rgblight_setrgb (0x00,  0xFF, 0xFF);
        break;   
    default: //  for any other layers, or the default layer
        rgblight_setrgb (0x33,  0xFF, 0x99);
        break;
    }
  return state;
};
*/